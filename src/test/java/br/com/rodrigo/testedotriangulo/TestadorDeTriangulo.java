/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.testedotriangulo;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Diamond
 */
public class TestadorDeTriangulo {
     @Test
    public void testEhUmTriangulo() {
        Triangulo triangulo = new Triangulo(10, 5, 5);
        Assert.assertTrue(triangulo.isTriangulo());
    }

    @Test
    public void testNaoEhTriangulo() {
        Triangulo triangulo = new Triangulo(10, 100, 1);
        Assert.assertFalse(triangulo.isTriangulo());
    }

    @Test
    public void ehEquilatero() {
        Triangulo triangulo = new Triangulo(10, 10, 10);
        Assert.assertTrue(triangulo.isEquilatero());
    }

    @Test
    public void ehIsoceles() {
        Triangulo triangulo = new Triangulo(10, 5, 10);
        Assert.assertTrue(triangulo.isIsoceles());
    }

    @Test
    public void ehEscaleno() {
        Triangulo triangulo = new Triangulo(1, 100, 10);
        Assert.assertTrue(triangulo.isEscaleno());
    }

}
