/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.testedotriangulo;

/**
 *
 * @author Diamond
 */
public class Triangulo {
      private double ladoA;
    private double ladoB;
    private double ladoC;

    public Triangulo() {
    }

    public Triangulo(double ladoA, double ladoB, double ladoC) {
        this.ladoA = ladoA;
        this.ladoB = ladoB;
        this.ladoC = ladoC;
    }

    public double getLadoA() {
        return ladoA;
    }

    public void setLadoA(double ladoA) {
        this.ladoA = ladoA;
    }

    public double getLadoB() {
        return ladoB;
    }

    public void setLadoB(double ladoB) {
        this.ladoB = ladoB;
    }

    public double getLadoC() {
        return ladoC;
    }

    public void setLadoC(double ladoC) {
        this.ladoC = ladoC;
    }

    public boolean isEquilatero() {
        return (this.ladoA == this.ladoB) && (this.ladoB == this.ladoC);
    }

    public boolean isIsoceles() {
        return (this.ladoA == this.ladoB) || (this.ladoB == this.ladoC) || (this.ladoC == this.ladoA);
    }

    public boolean isEscaleno() {
        return (this.ladoA != this.ladoB) && (this.ladoB != this.ladoC) && (this.ladoA != this.ladoC);
    }

    public boolean isTriangulo() {
        return isAEhMenorQueOutroDoisLados() && isBEhMenorQueOutroDoisLados() && isCEhMenorQueOutroDoisLados();
    }

    private boolean isAEhMenorQueOutroDoisLados() {
        return (this.ladoA <= (this.ladoB + this.ladoC));
    }//30 < 16+20

    private boolean isBEhMenorQueOutroDoisLados() {
        return (this.ladoB <= (this.ladoA + this.ladoC));
    }//20 < 16 + 30

    private boolean isCEhMenorQueOutroDoisLados() {
        return (this.ladoC <= (this.ladoA + this.ladoB));
    }//16 < 20+30
}
